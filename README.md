# excel-i18n

![从 excel 文件中提取多语言](./public/config.png)

```js
const pickSheet = require("excel-i18n").pickSheet;

// 保证多语言部分x和title的x对应，y和key的y对应
pickSheet({
    inputPath: "test/i18n.xlsx", // process.pwd下路径
    outputDir: "test/locale", // process.pwd下路径
    extension: ".ts",
    sheetName: "宝箱",
    keyX: "A", // key列序号
    keyY1: 5, // key列开头行号,从1开始
    keyY2: 14, // key列结束行号,从1开始
    titleX: ["D", "E"], // title 在哪些列
    titleY: 4, // title在哪一行,从1开始
    handleTitle: (position, content) => {
        if (content?.split("/")[1]) return content?.split("/")[1];
        console.log(content, "标题不合规范");
        process.exit();
    },
    handleContent(position, content) {
        if (
            postion === "D10" &&
            (!content ||
                !content.includes("{nickname}") ||
                !content.includes("{cash}"))
        ) {
            console.log(content, "占位符错误");
            process.exit();
        }
        return content;
    },
});
```

const commonjs = require("rollup-plugin-commonjs");
const typescript = require("@rollup/plugin-typescript");
const nodeResolve = require("rollup-plugin-node-resolve");
const inject = require("@rollup/plugin-inject");
const rollup = require("rollup");
const { terser } = require("rollup-plugin-terser");
const fs = require("fs/promises");
const path = require("path");

function clearDir(dir) {
    return fs
        .rm(path.resolve(process.cwd(), dir), { recursive: true })
        .catch(() => {});
}
async function build() {
    const bundle = await rollup.rollup({
        input: "src/index.ts",
        external: ["fs", "path", "os", "crypto", "stream", "util"],
        plugins: [
            commonjs(),
            nodeResolve({ include: "node_modules/**" }),
            typescript({
                tsconfig: "./tsconfig.json",
            }),
            inject({}),
        ],
    });
    await bundle.write({
        file: "./dist/index.js",
        format: "cjs",
        globals: {
            fs: "fs",
            path: "path",
            os: "os",
            crypto: "crypto",
            stream: "stream",
            util: "util",
        },
        sourcemap: true,
        sourcemapFile: "./dist/index.map",
        plugins: [terser({ format: { comments: false } })],
    });
    await bundle.close();
}
(async () => {
    await clearDir("dist");
    build();
})();
